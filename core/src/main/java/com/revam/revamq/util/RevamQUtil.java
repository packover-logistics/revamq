package com.revam.revamq.util;

import java.lang.instrument.Instrumentation;

public class RevamQUtil {

    public static <T> long getObjectSize(T t){
        return JsonUtil.convertAsString(t).getBytes().length;
    }
}
