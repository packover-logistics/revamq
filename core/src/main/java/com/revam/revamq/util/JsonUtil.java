package com.revam.revamq.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {

   private static ObjectMapper objectMapper = new ObjectMapper();

    public static <T> String convertAsString(T value){
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error while converting object to string");
        }
    }

    public static <T> T readAsObject(String value, TypeReference<T> typeReference) {
        try {
            return objectMapper.readValue(value,typeReference);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error while converting string to object");
        }
    }
}
