package com.revam.revamq.factory;

import com.revam.revamq.common.enums.QType;
import com.revam.revamq.common.model.QSetUp;
import com.revam.revamq.service.ILocalQ;
import com.revam.revamq.service.IMsgHistory;
import com.revam.revamq.service.impl.FifoLocalQ;
import com.revam.revamq.service.impl.StandardILocalQ;

public final class QTypeFactory {

    public static QTypeFactory INSTANCE;

    static {
        INSTANCE = new QTypeFactory();
    }

    private QTypeFactory(){

    }

    public ILocalQ getLocalQ(QSetUp qSetUp, IMsgHistory msgHistory){
        switch (qSetUp.getQType()){
            case FIFO:
                return new FifoLocalQ(qSetUp,msgHistory);
            case STANDARD:
                return new StandardILocalQ(qSetUp,msgHistory);
            default:
                return null;
        }
    }

}
