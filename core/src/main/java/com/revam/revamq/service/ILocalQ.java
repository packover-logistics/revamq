package com.revam.revamq.service;

import com.revam.revamq.common.model.LocalQMessage;
import com.revam.revamq.common.model.RevamQMessage;

import java.util.List;

public interface ILocalQ {

    void putMessage(String message);
    void putMessageWithDelay(LocalQMessage localQMessage, Integer delayInSec);
    List<RevamQMessage> getMessage();
    boolean deleteMessageFromQueue(String receiptHandle);
}
