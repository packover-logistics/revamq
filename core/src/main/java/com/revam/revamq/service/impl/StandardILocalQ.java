package com.revam.revamq.service.impl;


import com.revam.revamq.common.model.QSetUp;
import com.revam.revamq.service.IMsgHistory;

import java.util.concurrent.Executors;

public class StandardILocalQ extends AbstractLocalQ {

    private final static int NUMBER_OF_THREADS = 3;

    public StandardILocalQ(QSetUp qSetUp, IMsgHistory msgHistory) {
        super(qSetUp, Executors.newFixedThreadPool(NUMBER_OF_THREADS), msgHistory);
    }
}
