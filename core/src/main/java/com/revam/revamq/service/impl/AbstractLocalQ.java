package com.revam.revamq.service.impl;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.InvalidIdFormatException;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiptHandleIsInvalidException;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.util.CollectionUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.revam.revamq.common.enums.MsgStatus;
import com.revam.revamq.common.model.LocalQMessage;
import com.revam.revamq.common.model.QSetUp;
import com.revam.revamq.common.model.RevamQMessage;
import com.revam.revamq.service.ILocalQ;
import com.revam.revamq.service.IMsgHistory;
import com.revam.revamq.util.JsonUtil;
import com.revam.revamq.util.RevamQUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public abstract class AbstractLocalQ implements ILocalQ {
    private QSetUp qSetUp;
    private BlockingDeque<String> queue;
    private AmazonSQSClient amazonSQSClient;
    private ExecutorService executorService;
    private IMsgHistory msgHistory;

    private final static int SLEEP_TIME=1000;
    private final static int BATCH_MSG_SIZE=64000;
    private final static int MAX_MSG_SIZE=60000;

    protected AbstractLocalQ(QSetUp qSetUp, ExecutorService executorService, IMsgHistory msgHistory) {
        this.qSetUp = qSetUp;
        this.amazonSQSClient=qSetUp.getAmazonSQSClient();
        this.queue = new LinkedBlockingDeque<>(5000);
        this.executorService= executorService;
        this.msgHistory=msgHistory;
        Thread thread=new Thread(()->{
                   sqsExecutor();
        });
        thread.start();
    }

    private void sqsExecutor() {
        while(true){
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    createRevamMessageBatch();
                    try {
                        Thread.sleep(SLEEP_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }

    @Override
    public void putMessage(String message) {
        queue.add(message);
    }

    @Override
    public void putMessageWithDelay(LocalQMessage localQMessage, Integer delayInSec) {
        SendMessageRequest sendMessageRequest=new SendMessageRequest(qSetUp.getQUrl(), JsonUtil.convertAsString(localQMessage))
                .withDelaySeconds(delayInSec);
        amazonSQSClient.sendMessage(sendMessageRequest);
    }

    @Override
    public List<RevamQMessage> getMessage() {
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
        receiveMessageRequest.setMaxNumberOfMessages(qSetUp.getMaxNumberOfRevamQMessageReceive());
        receiveMessageRequest.setQueueUrl(qSetUp.getQUrl());
        receiveMessageRequest.setVisibilityTimeout(qSetUp.getRetryVisibilityTimeOut());
        List<Message> messages = qSetUp.getAmazonSQSClient().receiveMessage(receiveMessageRequest).getMessages();
        List<RevamQMessage> revamQMessageList = new ArrayList<>();
        if(CollectionUtils.isNullOrEmpty(messages)){
            revamQMessageList.add(new RevamQMessage());
            return revamQMessageList;
        }
        for(Message message : messages){
            RevamQMessage revamQMessage = JsonUtil.readAsObject(message.getBody(), new TypeReference<RevamQMessage>() {});
            revamQMessage.setReceiptHandle(message.getReceiptHandle());
            revamQMessageList.add(revamQMessage);
        }
        return revamQMessageList;
    }

    /**
     * @return Result of the DeleteMessage operation returned by the service.
     * @throws RuntimeException
     *         The specified receipt handle isn't valid for the current version.
     * @throws RuntimeException
     *         The specified receipt handle isn't valid.
     * @param receiptHandle
     * @return
     */
    @Override
    public boolean deleteMessageFromQueue(String receiptHandle) {
        DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest();
        deleteMessageRequest.setReceiptHandle(receiptHandle);
        deleteMessageRequest.setQueueUrl(qSetUp.getQUrl());
        try {
            qSetUp.getAmazonSQSClient().deleteMessage(deleteMessageRequest);
        }catch (InvalidIdFormatException invalidIdFormatException){
            throw new RuntimeException(invalidIdFormatException.getErrorMessage());
        }catch (ReceiptHandleIsInvalidException receiptHandleIsInvalidException){
            throw new RuntimeException(receiptHandleIsInvalidException.getErrorMessage());
        }
        return true;
    }
    private void pushMessageSQS(LocalQMessage localQMessage){
        SendMessageRequest sendMessageRequest=new SendMessageRequest(qSetUp.getQUrl(),JsonUtil.convertAsString(localQMessage.getRevamQMessage()));
        amazonSQSClient.sendMessage(sendMessageRequest);
    }

    private void createRevamMessageBatch(){
        LocalQMessage localQMessage= LocalQMessage.builder()
                .creationTime(System.currentTimeMillis())
                .revamQMessage(RevamQMessage.builder()
                        .messages(new ArrayList<>())
                        .build())
                .msgCount(0)
                .build();
        while(qSetUp.getMaxWaitInMilliSeconds()>(System.currentTimeMillis()-localQMessage.getCreationTime()) && RevamQUtil.getObjectSize(localQMessage)<BATCH_MSG_SIZE){
            try {
                String message = queue.poll(System.currentTimeMillis()-localQMessage.getCreationTime(), TimeUnit.MILLISECONDS);
                if(message.getBytes().length>MAX_MSG_SIZE){
                    LocalQMessage oneLocalQMessage= LocalQMessage.builder()
                            .creationTime(System.currentTimeMillis())
                            .revamQMessage(RevamQMessage.builder()
                                    .messages(new ArrayList<>())
                                    .build())
                            .msgCount(0)
                            .build();
                    oneLocalQMessage.setMsgCount(1);
                    oneLocalQMessage.getRevamQMessage().getMessages().add(message);
                    pushMessageSQS(localQMessage);
                    msgHistory.insertMsg(message,MsgStatus.INITIATED);
                    msgHistory.updateMsg(localQMessage.getRevamQMessage().getMessages(), MsgStatus.ADDED_TO_SQS);
                }
                else if(!Objects.isNull(message)) {
                    localQMessage.getRevamQMessage().getMessages().add(message);
                    localQMessage.setMsgCount(localQMessage.getMsgCount()+1);
                    msgHistory.insertMsg(message,MsgStatus.INITIATED);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(localQMessage.getMsgCount()>0) {
            pushMessageSQS(localQMessage);
            msgHistory.updateMsg(localQMessage.getRevamQMessage().getMessages(), MsgStatus.ADDED_TO_SQS);
        }
    }

}
