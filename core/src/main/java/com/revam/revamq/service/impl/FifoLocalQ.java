package com.revam.revamq.service.impl;


import com.revam.revamq.common.model.QSetUp;
import com.revam.revamq.service.IMsgHistory;

import java.util.concurrent.Executors;

public class FifoLocalQ extends AbstractLocalQ {

    public FifoLocalQ(QSetUp qSetUp, IMsgHistory msgHistory) {
        super(qSetUp, Executors.newSingleThreadExecutor(), msgHistory);
    }
}
