package com.revam.revamq.service;

import com.revam.revamq.common.enums.MsgStatus;

import java.util.List;


/**
*@implNote  It should be implemented to store msg history for recovery.
*@implNote  These method can be Async to optimise performance.

 */
public interface IMsgHistory {
    /**
    * @implNote msg will be save in to keep record what msg push to local db.

     */
    void insertMsg(String msg,MsgStatus status);

    void updateMsg(List<String> msges, MsgStatus status);

    void updateMsg(String msg,MsgStatus status);
}
