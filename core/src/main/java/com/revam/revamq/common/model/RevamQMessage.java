package com.revam.revamq.common.model;

import com.revam.revamq.util.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * collect message util 64kb
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RevamQMessage {
    private List<String> messages;
    private String receiptHandle;

    @Override
    public String toString() {
        return JsonUtil.convertAsString(this);
    }
}
