package com.revam.revamq.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Builder
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LocalQMessage {
    private RevamQMessage revamQMessage;
    private Integer delayInSec;
    private int msgCount;
    private Long creationTime;
    private Long sqsAddingTime;
}


