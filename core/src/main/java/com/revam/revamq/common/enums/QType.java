package com.revam.revamq.common.enums;

public enum QType {
    STANDARD("STANDARD"),
    FIFO("FIFO");
    String type;
    QType(String type){
        this.type=type;
    }
    public String getType(){
        return type;
    }
}
