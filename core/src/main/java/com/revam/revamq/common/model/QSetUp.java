package com.revam.revamq.common.model;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.revam.revamq.common.enums.QType;
import lombok.Data;

@Data
public class QSetUp {
    private Integer retryVisibilityTimeOut = 60*60;
    private String qUrl;
    private AmazonSQSClient amazonSQSClient;
    private long maxWaitInMilliSeconds;
    private Integer maxNumberOfRevamQMessageReceive;
    private QType qType;
}
