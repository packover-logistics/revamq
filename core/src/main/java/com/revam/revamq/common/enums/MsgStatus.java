package com.revam.revamq.common.enums;

public enum MsgStatus {
    INITIATED("INITIATED"),ADDED_TO_SQS("ADDED_TO_SQS"),FAILED_TO_ADD_IN_SQS("FAILED_TO_ADD_IN_SQS");
    private String status;
    MsgStatus(String status){
        this.status=status;
    }

    public String getStatus(){
        return  status;
    }
}
