package com.revam.revamq.client;

import com.revam.revamq.common.model.LocalQMessage;
import com.revam.revamq.common.model.QSetUp;
import com.revam.revamq.factory.QTypeFactory;
import com.revam.revamq.common.model.RevamQMessage;
import com.revam.revamq.service.ILocalQ;
import com.revam.revamq.service.IMsgHistory;

import java.util.List;
import java.util.Objects;

public class RevamQClient {

    private ILocalQ localQ;

    public RevamQClient(QSetUp qSetUp, IMsgHistory msgHistory) {
        Objects.requireNonNull(qSetUp);
        this.localQ = QTypeFactory.INSTANCE.getLocalQ(qSetUp,msgHistory);
    }

    public void putMessage(String message) {
        localQ.putMessage(message);
    }

    public void putMessageWithDelay(LocalQMessage message, Integer delayInSec) {
        localQ.putMessageWithDelay(message, delayInSec);
    }

    public List<RevamQMessage> getMessage() {
        return localQ.getMessage();
    }

    public boolean deleteMessageFromQueue(String receiptHandle) {
        return localQ.deleteMessageFromQueue(receiptHandle);
    }
}
