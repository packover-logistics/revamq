package com.revam.revamq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RevamQApplication {

    public static void main(String[] args) {
        SpringApplication.run(RevamQApplication.class, args);
    }

}

